# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This scripted object represents a revolute hinge. See: https://www.sky-engin.jp/en/MBDynTutorial/chap14/chap14.html

The syntax is: 

joint: <label>, 
      revolute hinge, 
         <node 1>,
            <relative offset 1>,
            euler, 0.,0.,0., #<relative orientation matrix>
         <node 2>,
            <relative offset 2>,
            euler, 0.,0.,0., #<relative orientation matrix>;

label: an integer number to identify the joint, eg: 1,2,3... 
node1: the label of the first structural node to which the joint is attached, eg: 1,2,3... 
relative offset: the possition of the joint relative to it's structural node. For the example in the above web page is '-0.5, 0.0, 0.0', because the node is at '0.5, 0.0, 0.0' relative to the absolute origin

Example:

joint: 2, 
      revolute hinge, 
         1,                                      # first node or body
            0.5, 0., 0.,                         # relative offset
            hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
         2,                                      # second node or body
            0., 0., -0.5,                        # relative offset
            hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation
       
       relative axis orientation and absolute pin orientation
       to rotate around x axis: euler, 0., pi/2., 0.
       to rotate around y axis: euler, pi/2., 0., 0.
'''

#from FreeCAD import Units
import FreeCAD
from sympy import Point3D, Line3D
import Draft

class Revolutehinge:
    def __init__(self, obj, label, node1, node2, reference, reference1):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the reference's center of mass, to define the orientation line:
            
        try:
            x4 = FreeCAD.Units.Quantity(reference.Curve.Center[0],FreeCAD.Units.Unit('mm'))  
            y4 = FreeCAD.Units.Quantity(reference.Curve.Center[1],FreeCAD.Units.Unit('mm'))  
            z4 = FreeCAD.Units.Quantity(reference.Curve.Center[2],FreeCAD.Units.Unit('mm'))  
        except:    
            x4 = FreeCAD.Units.Quantity(reference.CenterOfMass[0],FreeCAD.Units.Unit('mm'))  
            y4 = FreeCAD.Units.Quantity(reference.CenterOfMass[1],FreeCAD.Units.Unit('mm'))  
            z4 = FreeCAD.Units.Quantity(reference.CenterOfMass[2],FreeCAD.Units.Unit('mm')) 
       
        try:
            x3 = FreeCAD.Units.Quantity(reference1.Curve.Center[0],FreeCAD.Units.Unit('mm'))  
            y3 = FreeCAD.Units.Quantity(reference1.Curve.Center[1],FreeCAD.Units.Unit('mm'))  
            z3 = FreeCAD.Units.Quantity(reference1.Curve.Center[2],FreeCAD.Units.Unit('mm')) 
        except:
            x3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[0],FreeCAD.Units.Unit('mm'))  
            y3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[1],FreeCAD.Units.Unit('mm'))  
            z3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[2],FreeCAD.Units.Unit('mm')) 
        
        #Get the middle point, so that the joint is placed here:
        x = (x4+x3)/2
        y = (y4+y3)/2
        z = (z4+z3)/2
        
        #Calculate the relative offset 1:       
        x1 = x-node1.absolute_position_X
        y1 = y-node1.absolute_position_Y
        z1 = z-node1.absolute_position_Z
        
        #Calculate the relative offset 2:       
        x2 = x-node2.absolute_position_X
        y2 = y-node2.absolute_position_Y
        z2 = z-node2.absolute_position_Z

        obj.addExtension("App::GroupExtensionPython", self)          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","revolute hinge","label",1).label = label        
        obj.addProperty("App::PropertyString","node 1","revolute hinge","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","revolute hinge","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","joint","revolute hinge","joint",1).joint = 'revolute hinge'
        obj.addProperty("App::PropertyString","plugin variables","revolute hinge","plugin variables",1).plugin_variables = "none"

        #Absolute pin position:                
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
        
        #Relative offset 1:          
        obj.addProperty("App::PropertyString","relative offset 1 X","relative offset 1","relative offset 1 X",1).relative_offset_1_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Y","relative offset 1","relative offset 1 Y",1).relative_offset_1_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Z","relative offset 1","relative offset 1 Z",1).relative_offset_1_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Relative offset 2: 
        obj.addProperty("App::PropertyString","relative offset 2 X","relative offset 2","relative offset 2 X",1).relative_offset_2_X = str(round(x2.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Y","relative offset 2","relative offset 2 Y",1).relative_offset_2_Y = str(round(y2.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Z","relative offset 2","relative offset 2 Z",1).relative_offset_2_Z = str(round(z2.getValueAs('m').Value,precission))+" m"
                    
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'
        
        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self
        
        #Add Z vector of the coordinate system:      
        p1 = FreeCAD.Vector(x4, y4, z4)        
        p2 = FreeCAD.Vector(x3, y3, z3)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)        
        l.ViewObject.DrawStyle = u"Dashed"       
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
             
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label  
        
        #Rotation axis orientation: 
        p1, p2 = Point3D(x4.Value, y4.Value, z4.Value), Point3D(x3.Value, y3.Value, z3.Value)    
        l1 = Line3D(p1, p2)  
        obj.addProperty("App::PropertyString","relative orientation matrix 1","relative orientation matrix","relative orientation matrix 1",1).relative_orientation_matrix_1 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
        obj.addProperty("App::PropertyString","relative orientation matrix 2","relative orientation matrix","relative orientation matrix 2",1).relative_orientation_matrix_2 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                                                                         
        
    def execute(self, fp):
            precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
            
            ##############################################################################Calculate the new orientation: 
            ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line        
            #Two 3D points that define the joint´s line:
            p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
            l1 = Line3D(p1, p2)#Line that defines the joint
            #generate the orientation matrix:
            fp.relative_orientation_matrix_1 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
            fp.relative_orientation_matrix_2 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                       
            
            ##############################################################################Recalculate the offset, in case any of the two nodes was moved: 
            #get and update the pin position:
            x = FreeCAD.Units.Quantity((ZZ.Start[0] + ZZ.End[0])/2,FreeCAD.Units.Unit('mm')) 
            y = FreeCAD.Units.Quantity((ZZ.Start[1] + ZZ.End[1])/2,FreeCAD.Units.Unit('mm')) 
            z = FreeCAD.Units.Quantity((ZZ.Start[2] + ZZ.End[2])/2,FreeCAD.Units.Unit('mm')) 
            
            fp.absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
            fp.absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
            fp.absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
            
            #Move the force arrow:            
            Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
            p1 = FreeCAD.Vector(x, y, z)
            p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)
            FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0].Start=p1
            FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0].End=p2
            
            #get the node´s position:
            node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
            node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
            
            #Re-calculate the joint possition relative to it's nodes (relative offset)        
            x1 = x - node1.absolute_position_X
            y1 = y - node1.absolute_position_Y
            z1 = z - node1.absolute_position_Z
            
            x2 = x - node2.absolute_position_X
            y2 = y - node2.absolute_position_Y
            z2 = z - node2.absolute_position_Z
    
            #Update the offset:
            fp.relative_offset_1_X = str(round(x1.getValueAs('m').Value,precission))+" m"
            fp.relative_offset_1_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
            fp.relative_offset_1_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
            
            fp.relative_offset_2_X = str(round(x2.getValueAs('m').Value,precission))+" m"
            fp.relative_offset_2_Y = str(round(y2.getValueAs('m').Value,precission))+" m"
            fp.relative_offset_2_Z = str(round(z2.getValueAs('m').Value,precission))+" m"
    
            FreeCAD.Console.PrintMessage("REVOLUTE HINGE JOINT: " +fp.label+" successful recomputation...\n")

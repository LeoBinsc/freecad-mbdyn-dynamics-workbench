# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import FreeCADGui
#from PyQt4 import QtGui,QtCore
from PySide import QtGui,QtCore
import os
#import subprocess
from tospreadsheet import Tospreadsheet
from info import Infonode, Infojoint
from customviews import Beam3CustomView
from dynamics import Dynamics
#from sys import platform


dyn = Dynamics()

__dir__ = os.path.dirname(__file__)


############################################################################################################################################################
#
#  JOINTS:
#    
############################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////REVOLUTE PIN JOINT////////////////////////////////////////////////////"""
class _AddRevolutepinCmd:   
    def Activated(self): 
        try:                                   
            dyn.AddRevolutePin() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute pin joint.
                                          
Please meke sure you have selected one node and two valid reference geometries, in this order.""")
                     
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_pin_joint',
            'MBdyn_create_revolute_pin_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_pin_joint',
            """REVOLUTE PIN JOINT:            
    Select one node and two valid reference geometries, in this order, to create a revolute pin joint.
    This joint only allows the absolute rotation of a node about a given axis. 
    The rotation axis is defined by the line passing through the center of mass 
    (or the center point, in the case of arcs), of the two reference geometries provided.""")
        return {
            'Pixmap': __dir__ + '/icons/hinge.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevolutepin', _AddRevolutepinCmd()) 

"""//////////////////////////////////////////////////////////////////////////////REVOLUTE HINGE JOINT///////////////////////////////////////////////////"""
class _AddRevolutehingeCmd:   
    def Activated(self):
        try:
            dyn.AddRevoluteHingeJoint()              
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute hinge joint.
                                          
Please make sure you have selected two nodes and two valid reference geometries, in this order.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_hinge_joint',
            'MBdyn_create_revolute_hinge_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_hinge_joint',
            """REVOLUTE HINGE JOINT:        
    Select two nodes and two valid reference geometries, in this order, to create a revolute hinge joint.
    This joint only allows the relative rotation of two nodes about a given axis.
    The rotation axis is defined by the line passing through the center of mass 
    (or the center point, in the case of arcs), of the two reference geometries provided.""")
        return {
            'Pixmap': __dir__ + '/icons/hinge1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevolutehinge', _AddRevolutehingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CLAMP JOINT////////////////////////////////////////////////////////////////"""
class _AddClampCmd:    
    def Activated(self):
        try:
            dyn.AddClampJoint()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute clamp joint.
                                          
Please make sure you have selected only one node.""")        

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_clamp_joint',
            'MBdyn_add_clamp_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_clamp_joint',
            """CLAMP JOINT:
    Select only one node to create a clamp joint.
    This joint grounds all 6 degrees of freedom of a node in an arbitrary position and orientation that remains fixed.""")
        return {
            'Pixmap': __dir__ + '/icons/clamp.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddClamp', _AddClampCmd())  

"""//////////////////////////////////////////////////////////////////////////////AXIAL ROTATION JOINT//////////////////////////////////////////////////////"""
class _AddAxialRotationCmd:   
    def Activated(self):
        try:
            dyn.AddAxialRotationJoint()           
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the axial rotation joint.
                                          
Please make sure you have selected two nodes and two reference geometries.""")          
                                
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_axial_rotation_joint',
            'MBdyn_add_axial_rotation_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_axial_rotation_joint',
            """AXIAL ROTATION JOINT:
    Select two nodes and two valid reference geometries, in this order, to create an axial rotation joint.
    This joint is equivalent to a revolute hinge joint, but the angular velocity about the rotation axis is imposed by means of the driver.
    The rotation axis is defined by the line passing through the two centers of mass (or the center points, in the case of arcs), of the two reference geometries provided.""")
        return {
            'Pixmap': __dir__ + '/icons/axial.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AxialRotation', _AddAxialRotationCmd()) 

"""//////////////////////////////////////////////////////////////////////////////IN-LINE JOINT///////////////////////////////////////////////////////////"""
class _AddInLineCmd:   
    def Activated(self):
        try:
            dyn.AddInLineJoint()  
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the in-line joint.
                                          
Please make sure you have selected two nodes and two valid reference geometries, in this order.""")             
                
    def GetResources(self):        

        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_in_line_joint',
            'MBdyn_add_in_line_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_in_line_joint',
            """IN-LINE JOINT:
    Select two nodes and two valid reference geometries, in this order, to create an in-line joint.
    This joint forces a point relative to the second node to move along a line attached to the first node.
    The line is defined the two centers of mass (or the center points, in the case of arcs), of the two reference geometries provided.""")
        return {
            'Pixmap': __dir__ + '/icons/in-line.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddInLine', _AddInLineCmd()) 

"""//////////////////////////////////////////////////////////////////////////////PRISMATIC JOINT////////////////////////////////////////////////////////"""
class _AddPrismaticCmd:   
    def Activated(self):
        try:
            dyn.AddPrismaticJoint() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the prismatic joint.
                                          
Please make sure you have selected only two nodes.""")            
                              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_prismatic_joint',
            'MBDyn_add_prismatic_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_prismatic_joint',
            """PRISMATIC JOINT:
    Select only two nodes to create a prismatic joint.        
    This joints constrains the relative orientation of two nodes, so that their orientations remain parallel.
    The relative position is not constrained.""")
        return {
            'Pixmap': __dir__ + '/icons/prismatic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddPrismatic', _AddPrismaticCmd()) 

"""//////////////////////////////////////////////////////////////////////////////DRIVE HINGE JOINT////////////////////////////////////////////////////////"""
class _AddDrivehingeCmd:   
    def Activated(self):
        try:
            dyn.AddDriveHingeJoint() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive hinge joint.
                                          
Please make sure you have selected only two nodes.""")              

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Create_drive_hinge_joint',
            'MBdyn_Create_drive_hinge_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Create_drive_hinge_joint',
            """DRIVE HINGE JOINT:
    Select two nodes to create a drive hinge joint.
    This joint imposes the relative orientation between two nodes, in the form of a rotation about an axis whose amplitude is defined by a drive.
    Note: drive hinge joints are experimental; now they are more reliable, but are limited to "hinge_orientation" < pi. 
    Note: This element is superseded by the total joint.""")
        return {
            'Pixmap': __dir__ + '/icons/drivehinge.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDrivehinge', _AddDrivehingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////DEFORMABLE DISPLACEMENT JOINT////////////////////////////////////////////////////"""
class _AddDeformableDisplacementCmd:   
    def Activated(self):
        try:
            dyn.AddDeformableDisplacementJoint()            
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the deformable displacement joint.  
                                          
Please make sure you have selected two nodes and, optionally, two reference geometries.""")                                           
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Create_deformable_displacement_joint',
            'MBdyn_Create_deformable_displacement_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Create_deformable_displacement_joint',
            """DEFORMABLE DISPLACEMENT JOINT:
    Select two nodes, and optionally two reference geometries, to add a deformable displacement joint. 
    This joint implements a configuration dependent force that is exchanged between two points associated
    to two nodes with an offset. If only two nodes are selected, the offset becomes zero, and the points 
    are located at the nodes. The force may depend, by way of a generic 3D constitutive law, on the
    relative position and velocity of the two points, expressed in the reference frame of node 1.
    The constitutive law is attached to the reference frame of node 1, so the sequence of the connections
    may matter in case of anisotropic constitutive laws, if the relative orientation of the two nodes changes
    during the analysis.""")
        return {
            'Pixmap': __dir__ + '/icons/spring.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_DeformableDisplacement', _AddDeformableDisplacementCmd())

"""//////////////////////////////////////////////////////////////////////////////SPHERICAL HINGE///////////////////////////////////////////////////////////////////"""
class _AddSphericalHingeCmd: 
    def Activated(self):
        try:
            dyn.AddSphericalHinge()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the spherical hinge joint.  
                                          
Please make sure you have selected two nodes.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_spherical_hinge',
            'MBdyn_add_spherical_hinge')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_spherical_hinge',
            """SPHERICAL HINGE JOINT:  
    Select two nodes to create a spherical hinge joint.
    This joint constrains the relative position of two nodes; the relative orientation is not constrained.""")
        return {
            'Pixmap': __dir__ + '/icons/spherical.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddSphericalHinge', _AddSphericalHingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////IN-PLANE JOINT////////////////////////////////////////////////////////////////////"""
class _AddInPlaneCmd:   
    def Activated(self):
        try:
            dyn.AddInPlaneJoint()            
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the in-plane joint.  
                                          
Please make sure you have selected two nodes.""")                
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_in_plane_joint',
            'MBDyn_add_in_plane_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_in_plane_joint',
            """IN PLANE JOINT:  
    Select two nodes to create an in-plane joint.
    This joint forces a point relative to the second node to move in a plane attached to the first node.""")
        return {
            'Pixmap': __dir__ + '/icons/in-plane.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddInPlane', _AddInPlaneCmd()) 

"""//////////////////////////////////////////////////////////////////////////////TOTAL JOINT////////////////////////////////////////////////////"""
class _AddTotalJointCmd:   
    def Activated(self):
        try:
            dyn.AddTotalJoint() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the total joint.  
                                          
Please make sure you have selected two nodes.""")          

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_total_joint',
            'MBDyn_add_total_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_total_joint',
            """TOTAL JOINT:
    Select two nodes to create a total joint.
    This joint allows to arbitrarily constrain specific components of the relative position
    and orientation of two nodes. The value of the constrained components of the relative position and
    orientation can be imposed by means of drives. As such, this element allows to mimic the behavior of
    most ideal constraints that connect two nodes.""")
        return {
            'Pixmap': __dir__ + '/icons/total.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddTotalJoint', _AddTotalJointCmd())

"""//////////////////////////////////////////////////////////////////////////////REVOLUTE ROTATION JOINT////////////////////////////////////////////////////"""
class _AddRevoluterotationCmd:   
    def Activated(self):

        dyn.AddRevoluteRotation()                        

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_rotation_joint',
            'MBdyn_create_revolute_rotation_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_revolute_rotation_joint',
            """REVOLUTE ROTATION JOINT:
    Select two nodes and two reference geometries to create a revolute rotation joint.
    This joint allows the relative rotation of two nodes about a given axis. The relative position is not constrained.
    The rotation axis is defined by the line passing through the center of mass (or center point in the case of arcs), 
    of the two reference geometries provided.
    Note: A revolute joint without position constraints; this joint, in conjunction with an in-line joint, should be
    used to constrain, for example, the two nodes of a hydraulic actuator (see the cylindrical "compound" joint").""")
        return {
            'Pixmap': __dir__ + '/icons/revoluterotation.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevoluterotation', _AddRevoluterotationCmd()) 


############################################################################################################################################################
#
#  COMPOUND JOINTS:
#    
############################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////CONCIDENCE JOINT////////////////////////////////////////////////////"""
class _AddLockCmd:   
    def Activated(self):
        try:
            dyn.AddLockJoint()   
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the coincidence joint.
                                          
Please make sure you have selected two nodes.""")  
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_coincidence_joint',
            'MBDyn_add_coincidence_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_coincidence_joint',
            """COINCIDENCE JOINT:
   Select two nodes to add a coincidence joint.
   A spherical hinge joint and a prismatic joint will be added, restricting all 6 degrees of freedom.""")
        return {
            'Pixmap': __dir__ + '/icons/lock.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddLock', _AddLockCmd())  

"""//////////////////////////////////////////////////////////////////////////////CYLINDRICAL JOINT////////////////////////////////////////////////////"""
class _AddCylindricalCmd:   
    def Activated(self):
        try:
            dyn.AddCyindricalJoint()   
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the cylindrical joint.
                                          
Please make sure you have selected two nodes and two valid reference geometries.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_cylindrical_joint',
            'MBDyn_add_cylindrical_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_cylindrical_joint',
            """CYLINDRICAL JOINT:
   Select two nodes and two reference geometries to create a cylindrical joint.
   An in-line joint and a revolute rotation joint will be added.""")
        return {
            'Pixmap': __dir__ + '/icons/cylindrical.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddCylindrical', _AddCylindricalCmd())  

"""//////////////////////////////////////////////////////////////////////////////SLIDER JOINT//////////////////////////////////////////////////////////"""
class _AddSliderCmd:   
    def Activated(self):
        try:
            dyn.AddSliderJoint()   
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the slider joint.
                                          
Please make sure you have selected two nodes and two valid reference geometries.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_slider_joint',
            'MBDyn_add_slider_joint')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_slider_joint',
            """SLIDER JOINT:
   Select two nodes and two reference geometries to create a slider joint.
   An in-line joint and a prismatic joint will be added.""")                
        return {
            'Pixmap': __dir__ + '/icons/slider.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddSlider', _AddSliderCmd()) 

########################################################################################################################################################
#
#  Flexible:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////viscous body//////////////////////////////////////////////////////////"""
class _AddViscousBodyCmd:   
    def Activated(self):
        node = FreeCADGui.Selection.getSelection()[0]
        dyn.AddViscousBody(node) 

              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_viscous_body',
            'MBDyn_add_viscous_body')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_viscous_body',
            """VISCOUS BODY JOINT:
   Viscous body elements define a force and a moment that depend on the absolute linear and angular velocity of
   a body, projected in the reference frame of the node itself. The force and moment are defined as a 6D
   viscous constitutive law.
   Any of the supported 6D constitutive laws can be supplied to define the constitutive properties of each beam section.
   Select any number of structural nodes to apply viscous body elements.
   If a 6D cosntitutive law is selected after the nodes, this law will be applied to all the viscous body elements created.
   If no constitutive law is selected, one 6D cosntitutive law has to be manually asociated to each viscous body element.""")                
        return {
            'Pixmap': __dir__ + '/icons/honey.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddViscousBody', _AddViscousBodyCmd()) 

"""//////////////////////////////////////////////////////////////////////////////BEAM3//////////////////////////////////////////////////////////"""
class _AddBeam3Cmd:   
    def Activated(self):
        try:
            aux1 = int(0)
            
            existingbeams = 1
            for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
                if obj1.Label.startswith('Beam:'):
                    existingbeams = existingbeams+1
                    
            obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Beam: "+str(existingbeams))
            Beam3CustomView(obj.ViewObject)
            obj.Label = "Beam: "+str(existingbeams)
            FreeCAD.ActiveDocument.getObject("Beam3").addObject(obj)
            
            if FreeCADGui.Selection.getSelection()[-1].Label.startswith("structural:"):
                aux = len(FreeCADGui.Selection.getSelection())-1
            
                while aux1 < aux:
                    node1 = FreeCADGui.Selection.getSelection()[aux1]
                    node2 = FreeCADGui.Selection.getSelection()[aux1 + 1]
                    node3 = FreeCADGui.Selection.getSelection()[aux1 + 2]
                    dyn.AddBeam3(node1, node2, node3, str(existingbeams))  
                    aux1 = aux1 + 2
           
            if FreeCADGui.Selection.getSelection()[-1].Label.startswith("law:"):
                law = FreeCADGui.Selection.getSelection()[-1].Label
                aux = len(FreeCADGui.Selection.getSelection())-2
            
                while aux1 < aux:
                    node1 = FreeCADGui.Selection.getSelection()[aux1]
                    node2 = FreeCADGui.Selection.getSelection()[aux1 + 1]
                    node3 = FreeCADGui.Selection.getSelection()[aux1 + 2]
                    dyn.AddBeam3(node1, node2, node3, str(existingbeams), law)                     
                    aux1 = aux1 + 2
                    
            FreeCAD.ActiveDocument.recompute()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the beam3 element(s).
                                          
Please make sure you have selected an odd number of three or more nodes.
Alternatively, you can also select a 6D constitutive law after the nodes,
the selected law will be applied to all the beam3 elements created.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_beam3',
            'MBDyn_add_beam3')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_beam3',
            """BEAM3:
   Beam3 elements allow to model slender deformable structural components with a high level of flexibility.
   A 6D constitutive law must be provided, which defines the relationship between the strains, the curvatures
   of the beam and their time derivatives and the internal forces and moments at the evaluation points.
   Any of the supported 6D constitutive laws can be supplied to define the constitutive properties of each beam section.
   Select an odd number of three or more structural nodes to create one or a set of beam3 elements.
   If a 6D cosntitutive law is selected at the end of the nodes, this law will be applied to all the beam3 elements created
   If no constitutive law is selected, one 6D cosntitutive law has to be manually asociated to each beam3 element.""")                
        return {
            'Pixmap': __dir__ + '/icons/beam3.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddBeam3', _AddBeam3Cmd()) 

########################################################################################################################################################
#
#  Forces, couples and gravity:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL FORCE/////////////////////////////////////////////////////////////"""
class _AddStructuralForceCmd:    
    def Activated(self):
        try:
            dyn.AddStructuralForce()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the structural force.
                                          
Please make sure you have selected only one node.""")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_force',
            'MBdyn_add_structural_force')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_force',
            """STRUCTURAL FORCE:
    Select only one node to apply a structural force.""")
        return {
            'Pixmap': __dir__ + '/icons/force.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None
    
FreeCADGui.addCommand('MBdyn_AddStructuralForce', _AddStructuralForceCmd()) 

"""//////////////////////////////////////////////////////////////////////////////StructuralCouple////////////////////////////////////////////////////////////"""
class _AddStructuralCoupleCmd:    
    def Activated(self):
        try:
            dyn.AddStructuralCouple()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the structural couple.
                                          
Please make sure you have selected only one node.""")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_couple',
            'MBdyn_add_structural_couple')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_couple',
            """STRUCTURAL COUPLE:
    Select only one node to apply a structural couple.""")
        return {
            'Pixmap': __dir__ + '/icons/couple.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralCouple', _AddStructuralCoupleCmd()) 

"""//////////////////////////////////////////////////////////////////////////////GRAVITY///////////////////////////////////////////////////////////////////"""
class _AddGravityCmd:    
    def Activated(self):
        try:
            b = FreeCADGui.Selection.getSelection()
            if(len(b)==1):
                dyn.AddGravity(b[0])
    
            if(len(b)==0):
                dyn.AddGravity("NONE")
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding gravity to the simulation.
                                          
Please make sure that you have created a "WORLD" before you add gravity.

If you wish to create a uniform gravity, please make sure that nothing is selected.

If you wish to create a central gravity, please make sure you have selected only one solid object.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_gravity',
            'MBdyn_add_gravity')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_gravity',
            """GRAVITY: 
    If nothing is selected, a uniform gravitational pull will be aded.
    If a solid object is selected, a central gravity will be added at the center of mass of the object.""")
        return {
            'Pixmap': __dir__ + '/icons/apple.svg',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddGravity', _AddGravityCmd()) 

########################################################################################################################################################
#
#  SCALAR FUNCTION, DRIVE, CONSTITUTIVE LAW AND PLUGIN VARIABLE:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////CREATE A SCALAR FUNCTION////////////////////////////////////////////////////"""
class _AddScalarCmd:   
    def Activated(self):
        try:
            reply = QtGui.QInputDialog.getText(None,"Dynamics","Enter the number of steps (an integer):")
            if reply[1]:
                steps = int(reply[0])
                dyn.AddScalarFunction(steps)
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the scalar function.
                                          
Please make sure that you have created a "world" first.""")
                                                       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_scalar_function',
            'MBDyn_add_scalar_function')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_scalar_function',
            """SCALAR FUNCTION:
    Add a new scalar function to the simulation.""")
        return {
            'Pixmap': __dir__ + '/icons/scalar.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddScalar', _AddScalarCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CREATE A DRIVE////////////////////////////////////////////////////"""
class _AddDriveCmd:   
    def Activated(self):
        try:
            dyn.AddDrive()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive.
                                          
Please make sure that you have created a "world" first.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_drive',
            'MBDyn_add_drive')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_drive',
            """DRIVE:
    Add a drive to the simulation.""")
        return {
            'Pixmap': __dir__ + '/icons/drive.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDrive', _AddDriveCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CONSTITUTIVE LAW////////////////////////////////////////////////////"""
class _AddConstitutiveLawCmd:   
    def Activated(self):
        try:
            dyn.AddLaw()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the constitutive law.
                                          
Please make sure that you have created a "world" first.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_constitutive_law',
            'MBDyn_add_constitutive_law.')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_add_constitutive_law',
            """CONSTITUTIVE LAW:
    Add a new constitutive law to the simulation.""")
        return {
            'Pixmap': __dir__ + '/icons/law.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_ConstitutiveLaw', _AddConstitutiveLawCmd()) 

########################################################################################################################################################
#
#  TOOLS TO RELOCATE:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////RELOCATE TOOL////////////////////////////////////////////////////"""
class _RelocateCmd:   
    def Activated(self):
        try:
            dyn.Relocate()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the object.
                                          
Please make sure you have selected a node or an orientation line, and a reference geometry or a second node.""")            
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate',
            'MBDyn_relocate')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate',
            """RELOCATE 3D:
    Select a node or an orientation line and:
    a) a second node or,
    b) a reference geometry, such as a point, line, arc, face, or a 3D body,
    c) a set of bodies;
    the node, or the second point of the orientation line, will be placed at the center 
    of mass (or the center point, in the case of arcs) of the reference geometry.
    If multiple bodies were selected, the node, or the second point of the orientation line,
    will be placed at the barycenter.""")
        return {
            'Pixmap': __dir__ + '/icons/Place.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Relocate', _RelocateCmd()) 

"""//////////////////////////////////////////////////////////////////////////////RELOCATE X TOOL////////////////////////////////////////////////////"""

class _RelocateXCmd:   
    def Activated(self):
        try:
            dyn.RelocateX()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the X position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""") 
                  
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_X',
            'MBDyn_relocate_X')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_X',
            """RELOCATE X:
    Select:
        a) An orientation line. The line will be made parallel to the global X axis.
        b) Two nodes or one node and a reference geometry. The first node´s X position component will be made equal to
        the second node´s X position component; or to the X value of the reference´s center of mass or center point.""")
        return {
            'Pixmap': __dir__ + '/icons/Place-X.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateX', _RelocateXCmd())

"""//////////////////////////////////////////////////////////////////////////////RELOCATE-Y TOOL////////////////////////////////////////////////////"""
class _RelocateYCmd:   
    def Activated(self):
        try:
            dyn.RelocateY()     
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the Y position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""") 
                          
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_Y',
            'MBDyn_relocate_Y')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_Y',
            """RELOCATE Y:
    Select:
        a) An orientation line. The line will be made parallel to the global Y axis.
        b) Two nodes or one node and a reference geometry. The first node´s Y position component will be made equal to
        the second node´s Y position component; or to the Y value of the reference´s center of mass or center point.""")
        return {
            'Pixmap': __dir__ + '/icons/Place-Y.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateY', _RelocateYCmd()) 

"""//////////////////////////////////////////////////////////////////////////////RELOCATE-Z TOOL////////////////////////////////////////////////////"""
class _RelocateZCmd:   
    def Activated(self):
        try:
            dyn.RelocateZ() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the Z position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""")            
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_Z',
            'MBDyn_relocate_Z')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_relocate_Z',
            """RELOCATE Z:
    Select:
        a) An orientation line. The line will be made parallel to the global Z axis.
        b) Two nodes or one node and a reference geometry. The first node´s Z position component will be made equal to
        the second node´s Z position component; or to the Z value of the reference´s center of mass or center point.""")
        return {
            'Pixmap': __dir__ + '/icons/Place-Z.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateZ', _RelocateZCmd()) 

########################################################################################################################################################
#
#  VARIOUS TOOLS:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////EYE///////////////////////////////////////////////////////////////////////"""
class _ViewCmd:   
    def Activated(self):
        try:
            s = FreeCADGui.Selection.getSelection()
            if len(s)==1:
                dyn.Hide(FreeCADGui.Selection.getSelection()[0])        
            else:
                dyn.View()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while hiding the objects.
                                          
Please make sure that you have selected a node or a joint.""")
                          
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_view',
            'MBDyn_view')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MMBDyn_view',
            """VIEW:
   a) Select a joint to hide all the nodes and bodies exept those linked to the joint selected or,
   b) select a body to hide all the joints and bodies that are not connected to this body or,             
   c) select nohing to make all the joints and bodies visible.""")
        return {
            'Pixmap': __dir__ + '/icons/Eye.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_View', _ViewCmd()) 

"""///////////////////////////////////////////////////////////CREATE GLOBAL REFERENCE FRAME AND ADD CONTAINERS////////////////////////////////////////////"""
class _AddXYZCmd:    
    def Activated(self): 
        try:         
            dyn.CreateWorld();
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the world.""")       

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'Add global reference frame and containers',
            'Add global reference frame and containers')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'Add global reference frame and containers',
            """CREATE WORLD:
    a) Creates all the containers needed to organize the components of the MBD simulation. These are: 
    rigid bdies, materials, forces, couples, joints and nodes, etc.
    b) Adds an inertial global reference frame.
    c) Creates the parameters for the MBD simulation and animation.""")
        return {
            'Pixmap': __dir__ + '/icons/earth.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddXYZ', _AddXYZCmd())

"""//////////////////////////////////////////////////////////////////////////////SOLID BODY INFO////////////////////////////////////////////////////"""
class _CmCmd:  
    def Activated(self):
        try:
            reply = QtGui.QInputDialog.getText(None,"Dynamics","Provide the material's density, in kg/m^3:")
            if reply[1]:
                BaseBody = FreeCADGui.Selection.getSelection()[0]
                #Convert density into a FreeCAD unit, in kg/mm^3:
                density = FreeCAD.Units.Quantity(float(reply[0]),FreeCAD.Units.Unit('kg/mm^3'))
                dyn.Inspect(BaseBody, density)
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while calculating the physical properties of the selected body.
                                          
Please make sure you have selected only one body.
If the problem persists, the body model may be broken, 
for instance, it may be an empty shell, instead of a solid body:
empty shells do not have volume or mass. 
TIP: Check the body´s parametric geometry to ensure it is a closed shape.
TIP: Try to convert the body into a solid using using PART->CONVERT TO SOLID,
in the PART WORKBENCH.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_display_physical_properties_and_highlight_center_of_mass',
            'MBdyn_display_physical_properties_and_highlight_center_of_mass')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_display_physical_properties_and_highlight_center_of_mass',
            """PHYSICAL PROPERTIES:
    Select only one solid object to calculate its physical properties 
    (volume, mass and moments of inetia); and to highlight its absolute center of mass.
    NOTE: If the physical properties are not within the expected range, for instance if the
    mass or moments of inertia are too low or if the center of mass is not where it should be;
    this means that the body CAD is "broken". Check the body´s CAD parameters to ensure the body
    is not a massless empty shell, or the result of boolean operations involving empty shells.""")
        return {
            'Pixmap': __dir__ + '/icons/info1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Cm', _CmCmd())

"""//////////////////////////////////////////////////////////SHOW POSITION OF A NODE, JOINTOR BODY////////////////////////////////////////////////////"""
class _Cm1Cmd:  
    def Activated(self):
        X, Y, Z = 0, 0, 0 
        mX, mY, mZ = 0, 0, 0 
        try:
            length = FreeCAD.ActiveDocument.getObject("Line").End[0]
            
            if (len(FreeCADGui.Selection.getSelection()))==1:
                b = FreeCADGui.Selection.getSelection()
                #If a solid object was selected:
                x = b[0].Shape.Solids[0].CenterOfMass[0]
                y = b[0].Shape.Solids[0].CenterOfMass[1]
                z = b[0].Shape.Solids[0].CenterOfMass[2]                             
                #if a rigid body was selected    
                if(b[0].Label.startswith('body:')):
                    x = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_X).Value
                    y = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_Y).Value
                    z = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_Z).Value                                                    
    
                FreeCAD.ActiveDocument.cmx.X1=x+length
                FreeCAD.ActiveDocument.cmx.Y1=y
                FreeCAD.ActiveDocument.cmx.Z1=z
                FreeCAD.ActiveDocument.cmx.X2=x-length
                FreeCAD.ActiveDocument.cmx.Y2=y
                FreeCAD.ActiveDocument.cmx.Z2=z
                
                FreeCAD.ActiveDocument.cmy.X1=x
                FreeCAD.ActiveDocument.cmy.Y1=y+length
                FreeCAD.ActiveDocument.cmy.Z1=z
                FreeCAD.ActiveDocument.cmy.X2=x
                FreeCAD.ActiveDocument.cmy.Y2=y-length
                FreeCAD.ActiveDocument.cmy.Z2=z
                
                FreeCAD.ActiveDocument.cmz.X1=x
                FreeCAD.ActiveDocument.cmz.Y1=y
                FreeCAD.ActiveDocument.cmz.Z1=z+length
                FreeCAD.ActiveDocument.cmz.X2=x
                FreeCAD.ActiveDocument.cmz.Y2=y
                FreeCAD.ActiveDocument.cmz.Z2=z-length
                
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = True
            
            #if a set of objects were selected                        
            if (len(FreeCADGui.Selection.getSelection()))>1:
                
                for obj in range(0,len(FreeCADGui.Selection.getSelection())):
                    X = X + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[0] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Y = Y + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[1] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Z = Z + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[2] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))

                    mX = mX + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mY = mY + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mZ = mZ + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    
                x = X / mX
                y = Y / mY
                z = Z / mZ
        
                FreeCAD.ActiveDocument.cmx.X1=x+length
                FreeCAD.ActiveDocument.cmx.Y1=y
                FreeCAD.ActiveDocument.cmx.Z1=z
                FreeCAD.ActiveDocument.cmx.X2=x-length
                FreeCAD.ActiveDocument.cmx.Y2=y
                FreeCAD.ActiveDocument.cmx.Z2=z
                
                FreeCAD.ActiveDocument.cmy.X1=x
                FreeCAD.ActiveDocument.cmy.Y1=y+length
                FreeCAD.ActiveDocument.cmy.Z1=z
                FreeCAD.ActiveDocument.cmy.X2=x
                FreeCAD.ActiveDocument.cmy.Y2=y-length
                FreeCAD.ActiveDocument.cmy.Z2=z
                
                FreeCAD.ActiveDocument.cmz.X1=x
                FreeCAD.ActiveDocument.cmz.Y1=y
                FreeCAD.ActiveDocument.cmz.Z1=z+length
                FreeCAD.ActiveDocument.cmz.X2=x
                FreeCAD.ActiveDocument.cmz.Y2=y
                FreeCAD.ActiveDocument.cmz.Z2=z-length
                
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = True
                
            if (len(FreeCADGui.Selection.getSelection()))==0:
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = False
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = False
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = False
                
        except:
            print(QtGui.QMessageBox.information(None,"Error","Select only one structural node or one or more rigid bodies."))
                                            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_highlight_center_of_mass_or_barycenter',
            'MBdyn_highlight_center_of_mass_or_barycenter')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_highlight_center_of_mass_or_barycenter',
            """HIGHLIGHT CENTER OF MASS OR BARYCENTER:
    Select a solid object or a rigid body to highlight its center of mass.
    If multiple solid objects or rigid bodies are selected, the barycenter will be calculated and highlited.
    If nothing is selected, the marking lines will hide.""")
        return {
            'Pixmap': __dir__ + '/icons/center_of_mass.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Cm1', _Cm1Cmd())

"""////////////////////////////////////////////////////////////////////RECALCULATE THE HOLE MODEL////////////////////////////////////////////////////"""
class _RecalculateOrientation:    
    def Activated(self):
        for obj in FreeCAD.ActiveDocument.Objects:
            obj.touch()
            
        FreeCAD.ActiveDocument.recompute()   

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Recalculate_the_model',
            'MBdyn_Recalculate_the_model')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_Recalculate_the_model',
            """FORCE RECALCULATION:
    Forces the recalculation of the whole model.
    WARNING: Depending on the complexity of the MBD and the CAD models,
    the recalculation may require a considerable computation time.""")
        return {
            'Pixmap': __dir__ + '/icons/recalculate_node.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RecalculateOrientation', _RecalculateOrientation()) 

"""//////////////////////////////////////////////////////////////////////////////RANDOM COLOR////////////////////////////////////////////////////"""
class _RandomColorCmd:    
    def Activated(self):
        try:
            b = FreeCADGui.Selection.getSelection()
            dyn.RandomColor(b[0])   
        except:
            print(QtGui.QMessageBox.information(None,"Error", "Select only one rigid body."))
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_give_a_random_color_to_a_rigid_body',
            'Gives a random color to a rigid body.')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_give_a_random_color_to_a_rigid_body',
            """RANDOM COLOR:
    Select only one rigid body to give it a random color.
    It´s transparency will not change.""")
        return {
            'Pixmap': __dir__ + '/icons/colors.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RandomColor', _RandomColorCmd()) 

"""//////////////////////////////////////////////////////////////////////////////REDIRECTION TOOL////////////////////////////////////////////////////"""
class _RedirectionCmd:   
    def Activated(self):
        try: 
            dyn.Redirection()
        except:
            print(QtGui.QMessageBox.information(None,"Error", "Select only one orientation vector."))
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_redirection',
            'MBDyn_redirection')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_redirection',
            """FLIP SENSE:
    Select only one orientation vector to flip (invert) it´s sense. 
    The vector´s direction will be kept constant""")
        return {
            'Pixmap': __dir__ + '/icons/plusminus.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Redirection', _RedirectionCmd()) 

########################################################################################################################################################
#
#  TOOLS TO MANIPULATE JOINTS AND NODES:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////PLUGIN VARIABLE////////////////////////////////////////////////////"""
class _AddPuginVariableCmd:   
    def Activated(self):
        s = FreeCADGui.Selection.getSelection()
        if len(s)==1:
            dyn.AddPlugin(FreeCADGui.Selection.getSelection()[0])        
        else:
            pass
                         
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_plugin_variable',
            'MBDyn_plugin_variable')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBDyn_plugin_variable',
            """PLUGIN VARIABLE:
    Select a node or a joint to include a plugin variable.""")
        return {
            'Pixmap': __dir__ + '/icons/plugin.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddPuginVariable', _AddPuginVariableCmd()) 


"""//////////////////////////////////////////////////////////////////////////////INFO////////////////////////////////////////////////////"""
class _InfoCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==0):
            QtGui.QMessageBox.information(None,"Error","Select a node or a joint first.")
        else:
            if(b[0].Label.startswith('structural:')):
                node = int(b[0].label)
                Infonode(node)#Show node's info
            
            if(b[0].Label.startswith('joint:')):
                joint = int(b[0].label)
                Infojoint(joint)#Show joint's info


    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_export_summarized_simulation_results_to_a_FreeCAD_spreadsheet',
            'Export summarized simulation results to a FreeCAD spreadsheet')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_export_summarized_simulation_results_to_a_FreeCAD_spreadsheet',
            'Export summarized simulation results to a FreeCAD spreadsheet')
        return {
            'Pixmap': __dir__ + '/icons/spread1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Info', _InfoCmd())

"""//////////////////////////////////////////////////////////////////////////////SPREADSHEET////////////////////////////////////////////////////"""
class _SpreadsheetCmd:  
    def Activated(self):
        if(len(FreeCADGui.Selection.getSelection())==0):
            QtGui.QMessageBox.information(None,"Error","Select a node or a joint first.")
        else:
            choice = QtGui.QMessageBox.question(None,'Continue?',"Depending on your computer's resources and the simulation resolution, this may take some time. Hint: you can reduce the simulation resolution by increasing the time_step or reducing the final_time of the MBDyn object, in the MBDyn_simulation container. Continue?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        if choice == QtGui.QMessageBox.Yes:
            Tospreadsheet(FreeCADGui.Selection.getSelection()[0])
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_export_full_simulation_results_to_a_FreeCAD_spreadsheet',
            'Export full simulation results to a FreeCAD spreadsheet')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_export_full_simulation_results_to_a_FreeCAD_spreadsheet',
            'Export full simulation results to a FreeCAD spreadsheet')
        return {
            'Pixmap': __dir__ + '/icons/spread.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Spreadsheet', _SpreadsheetCmd())
"""//////////////////////////////////////////////////////////////////////////////START ANIMATION////////////////////////////////////////////////////"""
class _Animate1Cmd:  
    def Activated(self):
        #dyn.WriteInputFile()
        #dyn.Run()        
        dyn.StartAnimation()
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_start_animation',
            """Animate.""")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_start_animation',
            """ANIMATE:
    Click here to animate the MBD simulation results. 
    Make sure you execute MBDyn before you animate the model.""")
        return {
            'Pixmap': __dir__ + '/icons/play1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Animate1', _Animate1Cmd()) 
"""//////////////////////////////////////////////////////////////////////////////STOP ANIMATION////////////////////////////////////////////////////"""
class _AnimateStopCmd:  
    def Activated(self):
        dyn.StopAnimation()        
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_stop_animation',
            'Stop.')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_stop_animation',
            """STOP ANIMATION:
    Click here to stop the animation.""")
        return {
            'Pixmap': __dir__ + '/icons/stop.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('AnimateStopCmd', _AnimateStopCmd()) 
"""//////////////////////////////////////////////////////////////////////////////RESTORE ALL BODIES AND VECTORS TO THEIR POSSITIONS////////////////////////////////////////////////////"""
class _RestoreCmd:  
    def Activated(self):
        dyn.RestoreScene()
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_restore_the_3D_scene',
            'Restore.')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_restore_the_3D_scene',
            """RESTORE SCENE:
    Click here to restore the 3D scene.""")
        return {
            'Pixmap': __dir__ + '/icons/restore.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Restore', _RestoreCmd()) 
"""//////////////////////////////////////////////////////////////////////////////EXECUTE MBDYN////////////////////////////////////////////////////"""
class _RunCmd:  
    def Activated(self):
        #dyn.WriteInputFile()
        dyn.Run()
       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_execute_mbdyn_simulation',
            """Execute MBDyn.""")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_execute_mbdyn_simulation',
"""EXECUTE MBDYN:
    Click here to execute the MBDyn simulation from the input file. 
    Please make sure you generate the input file before executing MBDyn.""")
        return {
            'Pixmap': __dir__ + '/icons/play.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Run', _RunCmd())  


"""//////////////////////////////////////////////////////////////////////////////WRITE INPUT FILE////////////////////////////////////////////"""
class _AddMBDCmd:
    def Activated(self):
        dyn.WriteInputFile()

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_write_mbdyn_input_file',
            'Generate input file.')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_write_mbdyn_input_file',
            """INPUT FILE:
    Click here to generate the input file for MBDyn.
    You will find the input file within the "MBDyn_simulation" container, in the tree view.""")
        return {
            'Pixmap': __dir__ + '/icons/mbd.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddMBD', _AddMBDCmd()) 


"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL STATIC NODE///////////////////////////////////////////"""
class _AddStructuralStaticCmd:    
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==0):
            QtGui.QMessageBox.information(None,"","Select a solid object first.")
        else:
            body = FreeCADGui.Selection.getSelection()[0]    
            dyn.AddStructuralStaticNode(body) 
    
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_static_node',
            "Add static node.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_static_node',
            """Select only one solid object and click here to add a structural static node.
The static node will be placed at the center of mass of the solid object and parallel to the global reference frame.
You can afterwards change the initial position and orientation of the node, if needed.""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralStatic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralStatic', _AddStructuralStaticCmd()) 
"""//////////////////////////////////////////////////////////////////////////////PLOT////////////////////////////////////////////////////"""
class _PlotCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)!=1):
            QtGui.QMessageBox.information(None,"Error","Select one node or one joint first.")
        else:
            if(b[0].Label.startswith('structural:')):
                node = int(b[0].label)
                reply = QtGui.QInputDialog.getText(None,"Dynamics","Enter plot expression:")
                dyn.PlotNode(node, reply[0])#Plot all the data contained in the .mov file
            
            if(b[0].Label.startswith('joint:')):
                joint = int(b[0].label)
                reply = QtGui.QInputDialog.getText(None,"Dynamics","Enter plot expression:")
                dyn.PlotJoint(joint, reply[0])#Plot all the data contained in the .mov file

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_plot_simulation_results',
"Plot simulation results.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_plot_simulation_results',
"""Select a node and type one of the next plot expressions:\n
            "P(t)" or "Px(t)", "Py(t)", "Pz(t)" to plot the node´s position, or any of its components, as function of time.
            "Py(Px)", "Pz(Px)", "Px(Py)", etc, to plot one component of the node´s position as function of any other component.
            "P3D" to plot the node´s trajectory in a 3D plot.\n
            Use "V" instead of "P" in any of the plot expressions above, to plot the node´s velocity.
            Use "W" instead of "P" in any of the plot expressions above, to plot the node´s angular velocity.\n
            Use "O(t)" or "yaw(t)", "pitch(t)", "roll(t)" to plot the node´s orientation, or any of its components, as function of time.
            Use "yaw(pitch)", "yaw(roll)", "roll(yaw)", etc, to plot any component of the node´s orientation as function of any another component.
            Use "O3D" to plot the node´s orientation in a 3D plot.\n
            Use "ALL" to plot the all node´s results in a single plot.\n
Or, select a joint and type one of the next plot expressions:\n
            "F(t)" or "Fx(t)", "Fy(t)", "Fz(t)" to plot the joint´s local reaction force, or any of its components, as function of time.
            "Fy(Fx)", "Fz(Fx)", "Fx(Fy)", etc, to plot any component of the node´s local reaction force as function of any another component.
            "F3D" to plot the node´s local reaction forces in a 3D plot.\n
            Use "FF" instead of "F"" in any of the plot expressions above, to plot the reaction forces relative to the global reference frame.
            Use "T" instead of "F"" in any of the plot expressions above, to plot the reaction torque over the joint.\n
            Use "ALL"" to plot the all joint´s results in a single plot.""")
        return {
            'Pixmap': __dir__ + '/icons/Matplotlib1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Plot', _PlotCmd())

"""//////////////////////////////////////////////////////////////////////////////PLOT SCALAR FUNCTIONS OR DRIVES////////////////////////////////////////////////////"""
class _PlotDriveCmd:  
    def Activated(self):          
        dyn.PlotDrive()#Plot all the data contained in the .mov file
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_plot_Drive',
"Select a scalar function or a drive to plot it.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_plot_Drive',
"""PLOT SCALAR FUNCTION OR DRIVE:
    Select a scalar function or a drive to plot it.""")
        return {
            'Pixmap': __dir__ + '/icons/Matplotlib.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_PlotDrive', _PlotDriveCmd())



"""//////////////////////////////////////////////////////////////////////////////STATIC BODY////////////////////////////////////////////////////"""
class _AddStaticBodyCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==1):    
            dyn.AddStaticBody(b[0])            
        else:
            QtGui.QMessageBox.information(None,"Dynamics", "Dummy bodies do not have mass or inertia. They are only required to visualize the motion of dummy nodes. Select a solid object first. A dummy body associated to it's respective dummy node will be created.")
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_dummy_static_body',
            """Add static body.""")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_dummy_static_body',
            """Select only one solid object and click here to create a static body. 
Unlike rigid bodies, static bodies do not have any physical property (density, mass or inertia moments).""")
        return {
            'Pixmap': __dir__ + '/icons/viga2.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStaticBody', _AddStaticBodyCmd())
"""//////////////////////////////////////////////////////////////////////////////DUMMY BODY////////////////////////////////////////////////////"""
class _AddDummyBodyCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==1):    
            dyn.AddDummyBody(b[0])
                    
        else:
            QtGui.QMessageBox.information(None,"Dynamics", "Dummy bodies do not have mass or inertia. They are only required to visualize the motion of dummy nodes. Select a solid object first. A dummy body associated to it's respective dummy node will be created.")
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_dummy_static_body',
            """Add static body.""")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_dummy_static_body',
            """Select only one solid object and click here to create a dummy body. 
Unlike rigid bodies, dummy bodies do not have any physical property (density, mass or inertia moments).""")
        return {
            'Pixmap': __dir__ + '/icons/viga1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDummyBody', _AddDummyBodyCmd())



 
"""//////////////////////////////////////////////////////////////////////////////RIGID BODY///////////////////////////////////////////""" 
class _AddRigidBodyCmd:
    def Activated(self):
        try:
            for obj in FreeCADGui.Selection.getSelection():
                dyn.AddRigidBody(obj)   
        except:
            QtGui.QMessageBox.information(None,"FreeDyn","A structural node provides the degrees of freedom for a rigid body but it does not define a body. To define a body, mass, center of mass, and moments of inertia are required. A rigid body carries this information.\n\n Hint: select a simple (non-parametric) solid object first, enter the material's density, and FreeDyn will calculate the body's volume, mass, and moments of inertia. The rigid body will inherit the shape of the original solid object, and will be placed in the Rigid_bodies container, within Bodies.")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_rigid_body',
            "MBdyn_create_rigid_body")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_rigid_body',
            """RIGID BODY:
    Select one or a group of solid objects and click here to create one or more rigid bodies. 
    Each rigid body will be given the average density of steel, then, 
    the mass and moments of inertia will be obtained from the CAD model. 
    You can afterwards change the body´s material, in order to change it´s
    physical properties.
    If the desired material is not in the list, the FreeCAD´s material editor can be launched
    to add new materials.""")           
        return {
            'Pixmap': __dir__ + '/icons/viga.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRigidBody', _AddRigidBodyCmd())  

"""//////////////////////////////////////////////////////////////////////////////GEAR (A RIGID BODY plus some gear properties (RADIUS, teeth, modulus, etc)///////////////////////////////////////////""" 
class _AddGearCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)!=1):
            QtGui.QMessageBox.information(None,"FreeDyn","A structural node provides the degrees of freedom for a rigid body but it does not define a body. To define a body, mass, center of mass, and moments of inertia are required. A rigid body carries this information.\n\n Hint: select a simple (non-parametric) solid object first, enter the material's density, and FreeDyn will calculate the body's volume, mass, and moments of inertia. The rigid body will inherit the shape of the original solid object, and will be placed in the Rigid_bodies container, within Bodies.")
        else:
            dyn.AddGear(b[0]) 

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_rigid_body',
            "Add rigid body.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_create_rigid_body',
            """GEAR:
    Select only one solid object and click here to create a gear. 
    A gear has all the properties of a rigid body plus number of teeth, 
    radius, module, and stiffnes coefficient of ground spring. 
    These variables can optionally be provided or left blank.""")           
        return {
            'Pixmap': __dir__ + '/icons/gear.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddGear', _AddGearCmd())  

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL DUMMY NODE////////////////////////////////////////////////////"""
class _AddDummyNodeCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()   
        if(len(b)==2):
            dyn.AddStructuralDumyNode(FreeCADGui.Selection.getSelection()[0],FreeCADGui.Selection.getSelection()[1])           
        else:
            QtGui.QMessageBox.information(None,"FreeDyn","Unlike dynamic nodes, a dummy node cannot assume any degree of freedom, and has to be attached to another node. Hint: select a structural dynamic node and a solid object firts. A dummy node will be attached to the selected dynamic node, and will be positioned at the center of mass of the solid object.")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_dummy_node',
            "Add dummy node.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_dummy_node',
            """Select one dynamic node and one reference solid object, in this order, and click here to add a dummy node.
The dummy node will be placed at the center of mass of the reference solid object, and attached to the dynamic node.""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralDummy.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDummyNode', _AddDummyNodeCmd())

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL DYNAMIC NODE///////////////////////////////////////////////////////////////"""
class _AddStructuralDynamicNodeCmd:  
    def Activated(self):
        try:
            for obj in FreeCADGui.Selection.getSelection():
                dyn.AddStructuralDynamicNode(obj)   
        except:
            QtGui.QMessageBox.information(None,"FreeDyn","A structural node is a point in space which has six degrees of freedom, three define its possition, using cartesian coordinates (x,y,z), and three define its orientation, using Euler angles (Yaw, Pitch, Roll). A structural node is dynamic when it has inertia (linear and angular momentum). Hint: select a simple (non-parametric) solid object first. A structural dynamic node will be created at the center of mass of the object, and added to the Dynamic_nodes container, inside Structural_nodes. You can change the node's initial conditions in the node's properties.")
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_dynamic_node',
            "Add structural dynamic node.")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MBdyn_add_structural_dynamic_node',
"""STRUCTURAL DYNAMIC NODE:
    Select one or a set of solid objects and click here to create one or several structural dynamic nodes. 
    The nodes will initially be placed at the center of mass of the solid object, and parallel to the global reference frame. 
    You can afterwards change the initial position and orientation, as well as set its initial conditions 
    (initial velocity and initial angular velocity).""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralDynamic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralDynamicNodeCmd', _AddStructuralDynamicNodeCmd())


"""//////////////////////////////////////////////////////////////////////////////Check for joint´s consistency//////////////////////////////////////////////////"""
class _AutoReOrganizeCmd:
    
    def Activated(self):          
        dyn.AutoReOrganize();

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'Checks for joint´s naming consistency.',
            """Checks for joint´s naming consistency.""")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'Checks for joint´s naming consistency.',
            """Checks for joint´s naming consistency.""")
        return {
            'Pixmap': __dir__ + '/icons/organize.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AutoReOrganize', _AutoReOrganizeCmd())


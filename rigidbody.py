# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This scripted object represents a rigid body
https://www.sky-engin.jp/en/MBDynTutorial/chap05/chap05.html
A structural node provides the degrees of freedom for a rigid body but it does not define a rigid body. 
To define a rigid body, information of mass, center of mass, and inertia tensor is required. 
It is an element called body that carries that information and it is defined in the elements block. 

The syntax is:

body:   <label>, 
        <node>, 
        <mass>, 
        <relative center of mass>, 
        <inertia matrix>;

where:

label: an integer number to identify the body, eg: 1,2,3... 
node: the label of the structural node to which the body belongs, eg: 1,2,3... 
mass: body's mass in kg
relative center of mass: the possition of the body's center of mass relative to it's structural node. 
At this stage, all the structural nodes are defined at the center of mass of their objects (obtained from the CAD part),
therefore for all the bodies the relative center of mass is 0,0,0
inertia matrix: contains the three inertia moments of the body. The inertia moments are calculated below, from the CAD shape. 

Rigid body scripted objects take the shape of the original 3D object (BaseBody). To animate the MBDyn simulation, the possitions
of all the rigid bodies are updated according to the values in the 
MBDynCase.mov file. See: https://www.sky-engin.jp/en/MBDynTutorial/chap07/chap07.html
'''

#from FreeCAD import Units
import FreeCAD

class Rigidbody:                  
    def __init__(self, obj, BaseBody):#density in kg/mm^3 
        #the rigid body label is the same as the BaseBody label:
        label =  BaseBody.Label
        #The body is initially created with a density of 7900 kg/m^3, which is equivalent to 7.9e-06 kg/mm^3:
        density = FreeCAD.Units.Quantity(7.9e-06,FreeCAD.Units.Unit('kg/mm^3'))
        #obtain object's volume in m^3:      
        volume = FreeCAD.Units.Quantity(BaseBody.Shape.Volume,FreeCAD.Units.Unit('mm^3'))         
        #calculate object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
        #Returns moments of inertia divided by density:
        inertia = BaseBody.Shape.Solids[0].MatrixOfInertia
        
        #Store inertia moments without mass in mm^5:
        ii11 = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        ii12 = FreeCAD.Units.Quantity(inertia.A[1],FreeCAD.Units.Unit('mm^5'))
        ii13 = FreeCAD.Units.Quantity(inertia.A[2],FreeCAD.Units.Unit('mm^5'))
        
        ii21 = FreeCAD.Units.Quantity(inertia.A[4],FreeCAD.Units.Unit('mm^5'))
        ii22 = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        ii23 = FreeCAD.Units.Quantity(inertia.A[6],FreeCAD.Units.Unit('mm^5'))
        
        ii31 = FreeCAD.Units.Quantity(inertia.A[8],FreeCAD.Units.Unit('mm^5'))
        ii32 = FreeCAD.Units.Quantity(inertia.A[9],FreeCAD.Units.Unit('mm^5'))
        ii33 = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))
        
        #Compute inertia moments with mass, in kg*mm^2:      
        i11 = FreeCAD.Units.Quantity(ii11*density,FreeCAD.Units.Unit('kg*mm^2'))
        i12 = FreeCAD.Units.Quantity(ii12*density,FreeCAD.Units.Unit('kg*mm^2'))
        i13 = FreeCAD.Units.Quantity(ii13*density,FreeCAD.Units.Unit('kg*mm^2'))
        
        i21 = FreeCAD.Units.Quantity(ii21*density,FreeCAD.Units.Unit('kg*mm^2'))
        i22 = FreeCAD.Units.Quantity(ii22*density,FreeCAD.Units.Unit('kg*mm^2'))
        i23 = FreeCAD.Units.Quantity(ii23*density,FreeCAD.Units.Unit('kg*mm^2'))        

        i31 = FreeCAD.Units.Quantity(ii31*density,FreeCAD.Units.Unit('kg*mm^2'))
        i32 = FreeCAD.Units.Quantity(ii32*density,FreeCAD.Units.Unit('kg*mm^2'))
        i33 = FreeCAD.Units.Quantity(ii33*density,FreeCAD.Units.Unit('kg*mm^2')) 
        
        #Compute absolute center of mass, relative to global frame, in mm:
        cmx = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        cmy = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        cmz = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
        #since initially the node will be placed at the center of mass, the relative center of mass is [0,0,0]mm:
        cmxx = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        cmyy = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        cmzz = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        
        #Give the object the capability to store other objects:
        obj.addExtension("App::GroupExtensionPython", self) 
        
        #Now the object is initialized with the correct units for MBDyn, these are:
            #mass in kilograms
            #center of mass in meters
            #inertia without mass in m^5 
            #inertia with mass in kg*m^2
            #volume in m^3
            
        #All the values are rounded to the number of decimal places defined by the user:
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Get the number of decimal places defined by the user
                
        #Rigid body identifiers:       
        obj.addProperty("App::PropertyString","label","rigid body","label",1).label = label
        obj.addProperty("App::PropertyString","type","rigid body","type",1).type = 'rigid'
        obj.addProperty("App::PropertyString","node","rigid body","node",1).node = label #The body's asociated structural node  
        
        #Rigid body physical quantities:         
        obj.addProperty("App::PropertyString","density","physical properties",'density',1).density = str(round(density.getValueAs('kg/m^3').Value,precission))+' kg/m^3'
        obj.addProperty("App::PropertyString","volume","physical properties",'To access this variable type "volume_" followed by the number of body, for instance "volume_1".',1).volume = str(round(volume.getValueAs('m^3').Value,precission))+' m^3'
        obj.addProperty("App::PropertyString","mass","physical properties",'To access this variable type "mass_" followed by the number of body, for instance "mass_1".',1).mass = str(mass.Value)+' kg' 
        obj.addProperty("App::PropertyString","material","physical properties","material",1).material = 'Steel-Generic'#Material´s name is generic steel
        
        #absolute center of mass is the center of mass relative to the absolute coordinate system:
        obj.addProperty("App::PropertyString","absolute center of mass X","absolute center of mass","absolute center of mass X",1).absolute_center_of_mass_X = str(round(cmx.getValueAs('m').Value,precission))+' m'
        obj.addProperty("App::PropertyString","absolute center of mass Y","absolute center of mass","absolute center of mass Y",1).absolute_center_of_mass_Y = str(round(cmy.getValueAs('m').Value,precission))+' m'
        obj.addProperty("App::PropertyString","absolute center of mass Z","absolute center of mass","absolute center of mass Z",1).absolute_center_of_mass_Z = str(round(cmz.getValueAs('m').Value,precission))+' m'
        
        #since initially the node is at the center of mass, the relative center of mass (relative to the node) is [0,0,0]:
        obj.addProperty("App::PropertyString","relative center of mass X","relative center of mass","relative center of mass X",1).relative_center_of_mass_X = str(round(cmxx.getValueAs('m').Value,precission))+' m'       
        obj.addProperty("App::PropertyString","relative center of mass Y","relative center of mass","relative center of mass Y",1).relative_center_of_mass_Y = str(round(cmyy.getValueAs('m').Value,precission))+' m'    
        obj.addProperty("App::PropertyString","relative center of mass Z","relative center of mass","relative center of mass Z",1).relative_center_of_mass_Z = str(round(cmzz.getValueAs('m').Value,precission))+' m'     
        
        #Moments of inertia with mass:
        obj.addProperty("App::PropertyString","i11","moments of inertia with mass",'To access this variable type "i11_" followed by the number of body, for instance "i11_1".',1).i11 = str(round(i11.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        obj.addProperty("App::PropertyString","i12","moments of inertia with mass",'To access this variable type "i12_" followed by the number of body, for instance "i12_1".',1).i12 = str(round(i12.getValueAs('kg*m^2').Value,precission))+' kg*m^2'  
        obj.addProperty("App::PropertyString","i13","moments of inertia with mass",'To access this variable type "i13_" followed by the number of body, for instance "i13_1".',1).i13 = str(round(i13.getValueAs('kg*m^2').Value,precission))+' kg*m^2'

        obj.addProperty("App::PropertyString","i21","moments of inertia with mass",'To access this variable type "i21_" followed by the number of body, for instance "i21_1".',1).i21 = str(round(i21.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        obj.addProperty("App::PropertyString","i22","moments of inertia with mass",'To access this variable type "i22_" followed by the number of body, for instance "i22_1".',1).i22 = str(round(i22.getValueAs('kg*m^2').Value,precission))+' kg*m^2'  
        obj.addProperty("App::PropertyString","i23","moments of inertia with mass",'To access this variable type "i23_" followed by the number of body, for instance "i23_1".',1).i23 = str(round(i23.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        
        obj.addProperty("App::PropertyString","i31","moments of inertia with mass",'To access this variable type "i31_" followed by the number of body, for instance "i31_1".',1).i31 = str(round(i31.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        obj.addProperty("App::PropertyString","i32","moments of inertia with mass",'To access this variable type "i32_" followed by the number of body, for instance "i32_1".',1).i32 = str(round(i32.getValueAs('kg*m^2').Value,precission))+' kg*m^2'  
        obj.addProperty("App::PropertyString","i33","moments of inertia with mass",'To access this variable type "i33_" followed by the number of body, for instance "i33_1".',1).i33 = str(round(i33.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        
        #Moments of inertia without mass:
        obj.addProperty("App::PropertyString","ii11","moments of inertia without mass (divided by density)","ii11",1).ii11 = str(round(ii11.getValueAs('m^5').Value,precission))+' m^5'
        obj.addProperty("App::PropertyString","ii12","moments of inertia without mass (divided by density)","ii12",1).ii12 = str(round(ii12.getValueAs('m^5').Value,precission))+' m^5'   
        obj.addProperty("App::PropertyString","ii13","moments of inertia without mass (divided by density)","ii13",1).ii13 = str(round(ii13.getValueAs('m^5').Value,precission))+' m^5'   
        
        obj.addProperty("App::PropertyString","ii21","moments of inertia without mass (divided by density)","ii21",1).ii21 = str(round(ii21.getValueAs('m^5').Value,precission))+' m^5'
        obj.addProperty("App::PropertyString","ii22","moments of inertia without mass (divided by density)","ii22",1).ii22 = str(round(ii22.getValueAs('m^5').Value,precission))+' m^5'   
        obj.addProperty("App::PropertyString","ii23","moments of inertia without mass (divided by density)","ii23",1).ii23 = str(round(ii23.getValueAs('m^5').Value,precission))+' m^5'   

        obj.addProperty("App::PropertyString","ii31","moments of inertia without mass (divided by density)","ii31",1).ii31 = str(round(ii31.getValueAs('m^5').Value,precission))+' m^5'
        obj.addProperty("App::PropertyString","ii32","moments of inertia without mass (divided by density)","ii32",1).ii32 = str(round(ii32.getValueAs('m^5').Value,precission))+' m^5'   
        obj.addProperty("App::PropertyString","ii33","moments of inertia without mass (divided by density)","ii33",1).ii33 = str(round(ii33.getValueAs('m^5').Value,precission))+' m^5'   

        obj.Proxy = self
                
    def execute(self, fp):
        #Get the new precission:
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        #The label is used to obtain other objects related to this body:
        label = fp.label
        #Get the shape from the corresponding parametric body, to calculate the inertia:
        basebody = FreeCAD.ActiveDocument.getObjectsByLabel(label)[0]
        fp.Shape = basebody.Shape              
        #The inertia matrix is the same of the object, because the object´s shape is inherited from the original body. Get the inertia matrix:
        inertia = fp.Shape.Solids[0].MatrixOfInertia
        #get the new density (defined by the user in the Material object):
        fp.density = FreeCAD.ActiveDocument.getObjectsByLabel("material: "+fp.label)[0].Material['Density']
        fp.material = FreeCAD.ActiveDocument.getObjectsByLabel("material: "+fp.label)[0].Material['Name']
        density = FreeCAD.Units.Quantity(float(fp.density.split(' ')[0])/1000.0**3,FreeCAD.Units.Unit('kg/mm^3'))#Convert density to the appropriate units, to calculate moments of inertia
        #get the new volume:
        volume = FreeCAD.Units.Quantity(fp.Shape.Volume,FreeCAD.Units.Unit('mm^3'))  
        #calculate the new object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
        #get the new inertia moments without mass:
        ii11 = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        ii12 = FreeCAD.Units.Quantity(inertia.A[1],FreeCAD.Units.Unit('mm^5'))
        ii13 = FreeCAD.Units.Quantity(inertia.A[2],FreeCAD.Units.Unit('mm^5'))

        ii21 = FreeCAD.Units.Quantity(inertia.A[4],FreeCAD.Units.Unit('mm^5'))
        ii22 = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        ii23 = FreeCAD.Units.Quantity(inertia.A[6],FreeCAD.Units.Unit('mm^5'))

        ii31 = FreeCAD.Units.Quantity(inertia.A[8],FreeCAD.Units.Unit('mm^5'))
        ii32 = FreeCAD.Units.Quantity(inertia.A[9],FreeCAD.Units.Unit('mm^5'))
        ii33 = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))

        #compute new inertia moments, in kg*mm^2: 
        i11 = FreeCAD.Units.Quantity(ii11*density,FreeCAD.Units.Unit('kg*mm^2'))
        i12 = FreeCAD.Units.Quantity(ii12*density,FreeCAD.Units.Unit('kg*mm^2'))
        i13 = FreeCAD.Units.Quantity(ii13*density,FreeCAD.Units.Unit('kg*mm^2'))

        i21 = FreeCAD.Units.Quantity(ii21*density,FreeCAD.Units.Unit('kg*mm^2'))
        i22 = FreeCAD.Units.Quantity(ii22*density,FreeCAD.Units.Unit('kg*mm^2'))
        i23 = FreeCAD.Units.Quantity(ii23*density,FreeCAD.Units.Unit('kg*mm^2'))

        i31 = FreeCAD.Units.Quantity(ii31*density,FreeCAD.Units.Unit('kg*mm^2'))
        i32 = FreeCAD.Units.Quantity(ii32*density,FreeCAD.Units.Unit('kg*mm^2'))
        i33 = FreeCAD.Units.Quantity(ii33*density,FreeCAD.Units.Unit('kg*mm^2'))
        
        #Compute new absolute center of mass, relative to global frame:
        cmx = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        cmy = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        cmz = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))        

        fp.mass = str(mass.Value)+' kg' #Update mass
        
        #Update moments of inertia with mass:
        fp.i11 = str(round(i11.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i12 = str(round(i12.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i13 = str(round(i13.getValueAs('kg*m^2').Value,precission))+' kg*m^2'

        fp.i21 = str(round(i21.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i22 = str(round(i22.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i23 = str(round(i23.getValueAs('kg*m^2').Value,precission))+' kg*m^2'

        fp.i31 = str(round(i31.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i32 = str(round(i32.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        fp.i33 = str(round(i33.getValueAs('kg*m^2').Value,precission))+' kg*m^2'
        
        #Update moments of inertia witout mass:
        fp.ii11 = str(round(ii11.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii11 = str(round(ii11.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii11 = str(round(ii11.getValueAs('m^5').Value,precission))+' m^5'  

        fp.ii21 = str(round(ii21.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii21 = str(round(ii21.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii21 = str(round(ii21.getValueAs('m^5').Value,precission))+' m^5'  

        fp.ii31 = str(round(ii31.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii31 = str(round(ii31.getValueAs('m^5').Value,precission))+' m^5'
        fp.ii31 = str(round(ii31.getValueAs('m^5').Value,precission))+' m^5'  
         
        #Update the absolute center of mass:
        fp.absolute_center_of_mass_X = str(round(cmx.getValueAs('m').Value,precission))+' m'
        fp.absolute_center_of_mass_Y = str(round(cmy.getValueAs('m').Value,precission))+' m'
        fp.absolute_center_of_mass_Z = str(round(cmz.getValueAs('m').Value,precission))+' m'
        
        #calculate the new relative center of mass, in case the node has been moved:          
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label))==1):#Only if the node has already been created
            #get the corresponding node's absolute possition:
            xcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_X.getValueAs('m').Value
            ycc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_Y.getValueAs('m').Value
            zcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_Z.getValueAs('m').Value
            #Update the body's relative center of mass position:
            fp.relative_center_of_mass_X = str(round(float(fp.absolute_center_of_mass_X.split(' ')[0]) - xcc, precission))+' m'
            fp.relative_center_of_mass_Y = str(round(float(fp.absolute_center_of_mass_Y.split(' ')[0]) - ycc, precission))+' m'
            fp.relative_center_of_mass_Z = str(round(float(fp.absolute_center_of_mass_Z.split(' ')[0]) - zcc, precission))+' m'
            
            FreeCAD.Console.PrintMessage("RIGID BODY: " +fp.label+ " successful recomputation...\n")
            
        else:#If there is no structural node asociated to the body, issue an error:
            FreeCAD.Console.PrintMessage("RIGID BODY: " +fp.label+ ': Warning, no structural node asociated to this body. Relative center of mass cannot be calculated.\n')


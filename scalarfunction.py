# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


import FreeCAD
import numpy as np

class ScalarFunction:
    def __init__(self, obj, label, steps): 
        
        obj.addExtension("App::GroupExtensionPython", self)
        
        InitialTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").initial_time[:-2])
        FinalTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").final_time[:-2])
        time = np.linspace(InitialTime, FinalTime, num=steps)
        values= np.zeros(steps)
        
        obj.addProperty("App::PropertyString","label","scalar function","label",1).label = label
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","scalar function","type")
        obj.type=['cubicspline: cubic natural spline interpolation between the set of points (insert the points in the text file within this object)',
                  'multilinear: multilinear interpolation between the set of points (insert the points in the text file within this object)',
                  'chebychev: Chebychev interpolation between the set of points (insert the points in the text file within this object)']
  
        obj.addProperty("App::PropertyEnumeration","extrapolation","scalar function","extrapolation")
        obj.extrapolation=['do not extrapolate', 'extrapolate']
                
        obj.Proxy = self

        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "points_"+ label)   
        obj.Label = 'points: '+ label
        
        #Add an initial scalar function:
        aux = 0
        for i in time:    
            obj.Text = obj.Text + str(i)+ ',   '+str(values[aux])+', \n'
            
        obj.Text = obj.Text[:-3]#Remove the last comma
                               
        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
        pass
            
            
                   

# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
     joint: 2, # joint label
            deformable displacement joint,
            1, # node 1
            null, # relative offset to 1 [m]
            2, # node 2
            null, # relative offset to 2 [m]
            linear elastic isotropic,
            1., # spring stiffness in Newtons per meter
            prestrain, single, 1., 1., 0., const, 0.0282842712474619; #direction of oscillation and spring natural lenght in meters
'''

from sympy import Point3D, Line3D
import FreeCAD
import Draft

class DeformableDisplacement:                  
    def __init__(self, obj, label, node1, node2):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z          
        
        x3 = node2.absolute_position_X
        y3 = node2.absolute_position_Y
        z3 = node2.absolute_position_Z 
       

        #Natural lenght is the distance between the two nodes:       
        aux = pow(pow((node2.absolute_position_X - node1.absolute_position_X),2) + pow((node2.absolute_position_Y - node1.absolute_position_Y),2) + pow((node2.absolute_position_Z - node1.absolute_position_Z),2),(0.5)) 
        lenght = FreeCAD.Units.Quantity(aux,FreeCAD.Units.Unit('mm')) 
        
        obj.addExtension("App::GroupExtensionPython", self)        
        
        obj.addProperty("App::PropertyString","joint","deformable displacement joint","joint",1).joint = "deformable displacement"    
        obj.addProperty("App::PropertyString","label","deformable displacement joint","label",1).label = str(label)
        obj.addProperty("App::PropertyString","node_1","deformable displacement joint","node_1",1).node_1 = str(node1.label)
        obj.addProperty("App::PropertyString","node_2","deformable displacement joint","node_2",1).node_2 = str(node2.label)
        obj.addProperty("App::PropertyString","constitutive law","deformable displacement joint","constitutive law").constitutive_law = "type here the label of a constitutive law"
        obj.addProperty("App::PropertyString","direction","deformable displacement joint","direction",1).direction = ""
        obj.addProperty("App::PropertyString","natural lenght","deformable displacement joint","natural lenght",1).natural_lenght = str(round(lenght.getValueAs('m').Value,precission))+" m"

        obj.addProperty("App::PropertyString","absolute position X","initial absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","initial absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","initial absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"

        #Relative offset 1:          
        obj.addProperty("App::PropertyString","relative offset 1 X","relative offset 1","relative offset 1 X",1).relative_offset_1_X = "0. m"
        obj.addProperty("App::PropertyString","relative offset 1 Y","relative offset 1","relative offset 1 Y",1).relative_offset_1_Y = "0. m"
        obj.addProperty("App::PropertyString","relative offset 1 Z","relative offset 1","relative offset 1 Z",1).relative_offset_1_Z = "0. m"
        
        #Relative offset 2: 
        obj.addProperty("App::PropertyString","relative offset 2 X","relative offset 2","relative offset 2 X",1).relative_offset_2_X = "0. m"
        obj.addProperty("App::PropertyString","relative offset 2 Y","relative offset 2","relative offset 2 Y",1).relative_offset_2_Y = "0. m"
        obj.addProperty("App::PropertyString","relative offset 2 Z","relative offset 2","relative offset 2 Z",1).relative_offset_2_Z = "0. m"

        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'
        obj.addProperty("App::PropertyString","frame","animation","frame").frame = 'local'
        obj.addProperty("App::PropertyString","animate","animation","animate").animate = 'false'
        
        obj.Proxy = self
         
        p1 = FreeCAD.Vector(x, y, z)
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(x3, y3, z3)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ str(label)
        #l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        #l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.DrawStyle = u"Dashed"         
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00          
        
        
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.Label = "jf: "+ str(label)
        #add the spring
        #h = FreeCAD.ActiveDocument.addObject("Part::Helix","sp: "+ str(label))
        #h.Label = "sp: "+ str(label)
        #h.Pitch=2.00
        #h.Height= pow (( pow((x1 - x),2) + pow((y1 - y),2) + pow((z1 - z),2) ), 1/2)
        #h.Radius=Llength.Value/10
        #h.Angle=0.00
        #h.Placement = FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(0,0,0), FreeCAD.Vector(0,0,0))
                                                
        
    def execute(self, fp):
            ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
            precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
            #Two 3D points that define the joint´s line:
            p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
            #A line that defines the joint´s orientation:
            l1 = Line3D(p1, p2)#Line that defines the joint

            #generate the orientation matrix (it has to be a unit vector, so that it does not alter the spring natural leght):
            magnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
            
            fp.direction = str(round(l1.direction[0]/magnitude,precission)) +", "+ str(round(l1.direction[1]/magnitude,precission)) + ", " + str(round(l1.direction[2]/magnitude,precission))
            
            FreeCAD.Console.PrintMessage("DEFORMABLE DISPLACEMENT JOINT: " +fp.label+" successful recomputation...\n") 
             


             

# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD

class Beam3:
    def __init__(self, obj, label, node_1, node_2, node_3):
        
        obj.addExtension("App::GroupExtensionPython", self)

        obj.addProperty("App::PropertyString","label","beam3","label",1).label = label
        obj.addProperty("App::PropertyString","node_1_label","beam3","node_1_label",1).node_1_label = node_1.label
        obj.addProperty("App::PropertyString","node_2_label","beam3","node_2_label",1).node_2_label = node_2.label
        obj.addProperty("App::PropertyString","node_3_label","beam3","node_3_label",1).node_3_label = node_3.label
        
        obj.addProperty("App::PropertyString","relative_offset_1","relative_offset","relative_offset_1").relative_offset_1 = "null"
        obj.addProperty("App::PropertyString","relative_offset_2","relative_offset","relative_offset_2").relative_offset_2 = "null"
        obj.addProperty("App::PropertyString","relative_offset_3","relative_offset","relative_offset_3").relative_offset_3 = "null"
        
        obj.addProperty("App::PropertyString","orientation_matrix_section_I","orientation_matrix","orientation_matrix_section_I",1).orientation_matrix_section_I = "euler, 0., 0., 0."
        obj.addProperty("App::PropertyString","orientation_matrix_section_II","orientation_matrix","orientation_matrix_section_II",1).orientation_matrix_section_II = "euler, 0., 0., 0."
        
        obj.addProperty("App::PropertyString","constitutive_law_section_I","6D constitutive law","constitutive_law_section_I").constitutive_law_section_I = ""
        obj.addProperty("App::PropertyString","constitutive_law_section_II","6D constitutive law","constitutive_law_section_II").constitutive_law_section_II = ""        
        
        obj.Proxy = self
        
    def execute(self, fp):
        '''TODO: Central gravities: recalculate the origin if the refernce body has been moved'''
        
        FreeCAD.Console.PrintMessage("BEAM: " +fp.label+ " successful recomputation...\n")